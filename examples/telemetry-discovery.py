#!/usr/bin/env python3

from pumpkin_supmcu.i2c import I2CDriverMaster, I2CLinuxMaster
from pumpkin_supmcu.supmcu import request_module_definition, get_values
### For I2CDriverMaster
# The serial port that the I2CDriver is connected to
#   COM2, /dev/ttyUSB3...

I2CDRIVER_PORT = ''
i2c_master = I2CDriverMaster(I2CDRIVER_PORT)

### For I2CLinuxMaster
# The I2C bus that the SupMCU module(s) is connected to
#   /dev/i2c-2 would be 2, /dev/i2c-5 would be 5...
#
# LINUX_I2C_BUS = -1
# i2c_master = I2CLinuxMaster(LINUX_I2C_BUS)


addresses = i2c_master.get_bus_devices()
print("Active I2C addresses: ", end='')
print(*(hex(addr) for addr in addresses), sep=" ")

module_definitions = []
for address in addresses:
    module_definitions.append((address, request_module_definition(i2c_master, address)))

for addr, mod_def in module_definitions:
    for idx, telem in mod_def.module_telemetry.items():
        # Testing whether the telemetry is simulatable because when it isn't,
        #   garbage data might be received.
        if telem.simulatable:
            items = get_values(i2c_master, addr, mod_def.cmd_name, idx, telem.format)
            for item in items:
                print(item.string_value, end=' ')
            print()
        else:
            print("Not simulatable")
