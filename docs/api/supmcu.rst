`supmcu` module API docs
====================================================

.. automodule:: pumpkin_supmcu.supmcu

Data Types
----------

.. automodule:: pumpkin_supmcu.supmcu.types

.. autoclass:: pumpkin_supmcu.supmcu.DataType
    :members:

.. autoclass:: pumpkin_supmcu.supmcu.TelemetryType
    :members:

.. autoclass:: pumpkin_supmcu.supmcu.TelemetryDataItem

    .. py:attribute:: data_type

        The :class:`pumpkin_supmcu.supmcu.DataType` stored in the `TelemetryDataItem`.

    .. py:attribute:: value

        The value parsed from the telemetry response.

    .. py:attribute:: string_value

        The value formatted as a string.


.. autoclass:: pumpkin_supmcu.supmcu.SupMCUHDR

    .. py:attribute:: ready

        Value of the `ready` flag in the SupMCU telemetry.

    .. py:attribute:: timestamp

        The value of the timestamp in the SupMCU telemetry response.

.. autoclass:: pumpkin_supmcu.supmcu.SupMCUTelemetry

    .. py:attribute:: header

        The `pumpkin_supmcu.supmcu.SupMCUHDR` object for the telemetry response.

    .. py:attribute:: items

        The list of `pumpkin_supmcu.supmcu.TelemetryDataItems` that make up the telemetry response.

.. autoclass:: pumpkin_supmcu.supmcu.SupMCUTelemetryDefinition

    .. py:attribute:: format

        The format string returned from `SUP/MOD:TEL? #,FORMAT` request.

    .. py:attribute:: idx

        The index number of the telemetry item

    .. py:attribute:: name

        The name of the telemetry item as returned from `SUP/MOD:TEL? #,NAME` request.

    .. py:attribute:: telemetry_length

        The length in bytes of the telemetry response from the SupMCU module.

.. autoclass:: pumpkin_supmcu.supmcu.SupMCUModuleDefinition

    .. py:attribute:: address

        The I2C address of the module.

    .. py:attribute:: cmd_name

        The name of the module as used in SCPI commands (e.g. Battery Module 2 is BM2).

    .. py:attribute:: module_telemetry

        The module telemetry definitions for all the module specific telemetry. This is a
        dictionary keyed by telemetry index to telemetry definition.

    .. py:attribute:: name

        The name of the SupMCU module.

    .. py:attribute:: supmcu_telemetry

        The SupMCU telemetry definitions that are common across all SupMCU modules. This is a
        dictionary keyed by telemetry index to telemetry definition.

.. autofunction:: pumpkin_supmcu.supmcu.sizeof_supmcu_type

.. autofunction:: pumpkin_supmcu.supmcu.typeof_supmcu_fmt_char

.. autofunction:: pumpkin_supmcu.supmcu.datatype_to_supmcu_fmt_char

Telemetry Parsing
-----------------

.. automodule:: pumpkin_supmcu.supmcu.parsing

.. autoclass:: pumpkin_supmcu.supmcu.DataItemParser
    :members:

.. data:: pumpkin_supmcu.supmcu.Parsers
    :annotation: Contains a mapping of SupMCU format character to appropriate parser for the format specifier.

.. autofunction:: pumpkin_supmcu.supmcu.parse_telemetry

.. autofunction:: pumpkin_supmcu.supmcu.parse_header

SupMCU Discovery
----------------

.. automodule:: pumpkin_supmcu.supmcu.discovery

.. autofunction:: pumpkin_supmcu.supmcu.request_module_definition

.. autofunction:: pumpkin_supmcu.supmcu.request_telemetry_definition

.. autofunction:: pumpkin_supmcu.supmcu.get_values

SupMCU interface
----------------

.. automodule:: pumpkin_supmcu.supmcu.i2c

.. autoclass:: pumpkin_supmcu.supmcu.SupMCUMaster
    :members:
