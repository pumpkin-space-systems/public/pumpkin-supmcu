`i2c` module API docs
====================================================

.. automodule:: pumpkin_supmcu.i2c

`i2c.master` module API docs
----------------------------

.. automodule:: pumpkin_supmcu.i2c.master

.. autoclass:: pumpkin_supmcu.i2c.I2CBusSpeed
    :members:

.. autoclass:: pumpkin_supmcu.i2c.I2CMaster
    :members:
