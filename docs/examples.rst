Examples
=========

.. warning::

    These examples assume that you have already installed pumpkin-supmcu with :code:`pip3 install pumpkin-supmcu` on python 3.6 or above.

.. note::

    Just want the code?  See `this folder <https://gitlab.com/pumpkin-space-systems/public/pumpkin-supmcu/-/tree/master/examples>`_


Retrieving Telemetry Data
-------------------------
This example will walk through initializing a :any:`I2CMaster` and using it to
get the current values of all the telemetry of each module connected to the I2C
bus.

.. code-block:: python3

    from pumpkin_supmcu.i2c import I2CDriverMaster, I2CLinuxMaster
    from pumpkin_supmcu.supmcu import request_module_definition, get_values

    PORT = 'COM4'

    i2c_master = I2CDriverMaster(PORT)

This code imports everything that is needed from pumpkin-supmcu and initializes
an I2C master.  :code:`PORT`'s value depends on what kind of I2C master is being
initialized.  For the :any:`I2CDriverMaster`, :code:`PORT` is what serial port
the I2CDriver is, and can be a COM port on windows or a /dev/ttyUSB* filename on
linux.  For the :any:`I2CLinuxMaster`, :code:`PORT` is the number of the linux
/dev/i2c-* interface.  For instance, if the module was connected to /dev/i2c-2
then :code:`PORT` should be set to 2.  Different I2C masters can be used
interchangeably, after their initialization.


.. code-block:: python3

    addresses = i2c_master.get_bus_devices()

    module_definitions = []
    for address in addresses:
        module_definitions.append((address, request_module_definition(i2c_master, address)))

This block gets the addresses of all active devices on the I2C bus, and then
uses :any:`request_module_definition`
to save a :any:`SupMCUModuleDefinition`
for each device.

.. code-block:: python3

    for addr, mod_def in module_definitions:
        for idx, telem in mod_def.supmcu_telemetry.items():
            if telem.simulatable:
                items = get_values(i2c_master, addr, mod_def.cmd_name, idx, telem.format)
                for item in items:
                    print(item.string_value, end=' ')
                print()
            else:
                print("Not simulatable, might be a garbage value")

This code iterates through each telemetry object in each module definition and
uses :any:`get_values` to print its values if it's simulatable.  When the
telemetry isn't simulatable, :any:`get_values` will return garbage data on
QSMs or STMs.
