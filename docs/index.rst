.. Pumpkin SupMCU Interface documentation master file, created by
   sphinx-quickstart on Tue Nov  5 17:47:45 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

`pumpkin-supmcu` documentation
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api/i2c
   api/supmcu
   examples

The `pumpkin_supmcu` package has the following functionality:

* Discover all telemetry items on a module, and put into telemetry definitions.
* Parse any telemetry item on a module given a module telemetry definition.
* Provide a universal I2C interface so we can integrate multiple I2C Masters.
* Request telemetry from any SupMCU module via I2C interface.
* Write commands to SupMCU modules via I2C interface.

The `pumpkin_supmcu.i2c` package provides :class:`~pumpkin_supmcu.i2c.I2CMaster` implementations for the
`I2CDriver Board <https://i2cdriver.com/>`_ or `(support coming soon)`
`Total Phase Aardvark Adaptor <https://www.totalphase.com/products/aardvark-i2cspi/>`_.

Users are encouraged to contribute more implementations of other I2CMaster devices as
`pumpkin_supmcu` provides a :class:`~pumpkin_supmcu.I2CMaster` :class:`~typing.Protocol` to implement
(see `PEP 544 <https://www.python.org/dev/peps/pep-0544/>`_)


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
